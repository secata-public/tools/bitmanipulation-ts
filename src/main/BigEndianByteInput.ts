import { ByteInput } from "./ByteInput";
import BN from "bn.js";

/**
 * Reader of an input stream, can read different types of basic types. Reads in big-endian format.
 */
export class BigEndianByteInput implements ByteInput {
  private readonly buffer: Buffer;
  private offset = 0;

  /**
   * Creates a new big-endian stream based on an input buffer.
   * @param buffer the buffer to wrap.
   */
  constructor(buffer: Buffer) {
    this.buffer = buffer;
  }

  /**
   * @inheritDoc
   */
  readBoolean(): boolean {
    const bool = this.buffer.readUInt8(this.offset) !== 0;
    this.offset++;
    return bool;
  }

  /**
   * @inheritDoc
   */
  readBytes(noBytes: number): Buffer {
    const buffer = this.buffer.slice(this.offset, this.offset + noBytes);
    this.offset += noBytes;
    if (buffer.length !== noBytes) {
      throw new Error("Unable to read bytes");
    }
    return buffer;
  }

  /**
   * @inheritDoc
   */
  readI8(): number {
    const byte = this.buffer.readInt8(this.offset);
    this.offset++;
    return byte;
  }

  /**
   * @inheritDoc
   */
  readI16(): number {
    const short = this.buffer.readInt16BE(this.offset);
    this.offset += 2;
    return short;
  }

  /**
   * @inheritDoc
   */
  readI32(): number {
    const int = this.buffer.readInt32BE(this.offset);
    this.offset += 4;
    return int;
  }

  /**
   * @inheritDoc
   */
  readI64(): BN {
    return this.readSignedBigInteger(8);
  }

  /**
   * @inheritDoc
   */
  readSignedBigInteger(noBytes: number): BN {
    const buffer = this.readBytes(noBytes);
    // Stryker disable next-line StringLiteral: is equivalent to new BN(buffer, ""), but "be" kept for clarity
    return new BN(buffer, "be").fromTwos(8 * noBytes);
  }

  /**
   * @inheritDoc
   */
  readU8(): number {
    const byte = this.buffer.readUInt8(this.offset);
    this.offset++;
    return byte;
  }

  /**
   * @inheritDoc
   */
  readU16(): number {
    const short = this.buffer.readUInt16BE(this.offset);
    this.offset += 2;
    return short;
  }

  /**
   * @inheritDoc
   */
  readU32(): number {
    const int = this.buffer.readUInt32BE(this.offset);
    this.offset += 4;
    return int;
  }

  /**
   * @inheritDoc
   */
  readU64(): BN {
    return this.readUnsignedBigInteger(8);
  }

  /**
   * @inheritDoc
   */
  readUnsignedBigInteger(noBytes: number): BN {
    const buffer = this.readBytes(noBytes);
    // Stryker disable next-line StringLiteral: is equivalent to new BN(buffer, ""), but "be" kept for clarity
    return new BN(buffer, "be");
  }

  /**
   * @inheritDoc
   */
  readString(): string {
    const stringLength = this.readI32();
    return this.readBytes(stringLength).toString("utf8");
  }

  /**
   * @inheritDoc
   */
  readRemaining(): Buffer {
    return this.readBytes(this.buffer.length - this.offset);
  }

  /**
   * @inheritDoc
   */
  public skipBytes(skip: number) {
    if (skip < 0) {
      throw new Error("Must skip a non negative number of bytes");
    }
    if (this.buffer.length < this.offset + skip) {
      throw new Error("Cannot skip past buffer size");
    }
    this.offset += skip;
  }
}
