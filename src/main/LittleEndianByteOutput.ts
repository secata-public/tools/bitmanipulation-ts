import { BN } from "./index";
import { ByteOutput } from "./ByteOutput";

/**
 * Writer to an output stream, can write different types of basic types. Writes in little-endian
 * format.
 */
export class LittleEndianByteOutput implements ByteOutput {
  private buffer: Buffer;

  /**
   * Creates a LittleEndianByteOutput, runs the serialization and returns the bytes.
   * @param serializer the serialization to happen
   * @returns the serialized bytes
   */
  public static serialize(serializer: (out: LittleEndianByteOutput) => void): Buffer {
    const out = new LittleEndianByteOutput();
    serializer(out);
    return out.toBuffer();
  }

  /**
   * Creates a new little-endian stream.
   */
  constructor() {
    this.buffer = Buffer.alloc(0);
  }

  /**
   * @inheritDoc
   */
  public writeBytes(values: Buffer): void {
    this.appendBuffer(values);
  }

  /**
   * @inheritDoc
   */
  public writeBoolean(value: boolean): void {
    const buffer = Buffer.alloc(1);
    buffer.writeUInt8(value ? 1 : 0, 0);
    this.appendBuffer(buffer);
  }

  /**
   * @inheritDoc
   */
  public writeI8(value: number): void {
    const buffer = Buffer.alloc(1);
    buffer.writeInt8(value, 0);
    this.appendBuffer(buffer);
  }

  /**
   * @inheritDoc
   */
  public writeI16(value: number): void {
    const buffer = Buffer.alloc(2);
    buffer.writeInt16LE(value, 0);
    this.appendBuffer(buffer);
  }

  /**
   * @inheritDoc
   */
  public writeI32(value: number): void {
    const buffer = Buffer.alloc(4);
    buffer.writeInt32LE(value, 0);
    this.appendBuffer(buffer);
  }

  /**
   * @inheritDoc
   */
  public writeI64(value: BN): void {
    this.writeSignedBigInteger(value, 8);
  }

  /**
   * @inheritDoc
   */
  public writeSignedBigInteger(value: BN, noBytes: number): void {
    if (value.byteLength() > noBytes) {
      throw new Error(
        `Cannot write BN as ${noBytes} bytes; requires at least ${value.byteLength()} bytes`
      );
    }
    const buffer = value.toTwos(noBytes * 8).toArrayLike(Buffer, "le", noBytes);
    this.appendBuffer(buffer);
  }

  /**
   * @inheritDoc
   */
  public writeU8(value: number): void {
    const buffer = Buffer.alloc(1);
    buffer.writeUInt8(value, 0);
    this.appendBuffer(buffer);
  }

  /**
   * @inheritDoc
   */
  public writeU16(value: number): void {
    const buffer = Buffer.alloc(2);
    buffer.writeUInt16LE(value, 0);
    this.appendBuffer(buffer);
  }

  /**
   * @inheritDoc
   */
  public writeU32(value: number): void {
    const buffer = Buffer.alloc(4);
    buffer.writeUInt32LE(value, 0);
    this.appendBuffer(buffer);
  }

  /**
   * @inheritDoc
   */
  public writeU64(value: BN): void {
    this.writeUnsignedBigInteger(value, 8);
  }

  /**
   * @inheritDoc
   */
  public writeUnsignedBigInteger(value: BN, noBytes: number): void {
    if (value.cmpn(0) === -1) {
      throw new Error("Value must be non negative");
    }
    if (value.byteLength() > noBytes) {
      throw new Error(
        `Cannot write BN as ${noBytes} bytes; requires at least ${value.byteLength()} bytes`
      );
    }
    const buffer = value.toArrayLike(Buffer, "le", noBytes);
    this.appendBuffer(buffer);
  }

  /**
   * @inheritDoc
   */
  public writeString(value: string): void {
    // Stryker disable next-line StringLiteral: is equivalent to Buffer.from(value, ""), but "utf8" kept for clarity
    const strBuffer = Buffer.from(value, "utf8");
    this.writeI32(strBuffer.length);
    this.appendBuffer(strBuffer);
  }

  /**
   * @inheritDoc
   */
  public toBuffer(): Buffer {
    return this.buffer;
  }

  private readonly appendBuffer = (buffer: Buffer) => {
    this.buffer = Buffer.concat([this.buffer, buffer]);
  };
}
