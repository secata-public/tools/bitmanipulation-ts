import { BN } from "./index";

/**
 * Common interface of an endian-aware writer to an output stream.
 */
export interface ByteOutput {
  /**
   * Writes a byte array.
   * @param values the byte array to write to the stream
   */
  writeBytes(values: Buffer): void;

  /**
   * Writes a boolean.
   * @param value the boolean to write to the stream
   */
  writeBoolean(value: boolean): void;

  /**
   * Writes a signed 8-bit number.
   * @param value the number to write to the stream
   */
  writeI8(value: number): void;

  /**
   * Writes a signed 16-bit number.
   * @param value the number to write to the stream
   */
  writeI16(value: number): void;

  /**
   * Writes a signed 32-bit number.
   * @param value the number to write to the stream
   */
  writeI32(value: number): void;

  /**
   * Writes a signed 64-bit number.
   * @param value the BN to write to the stream
   */
  writeI64(value: BN): void;

  /**
   * Writes a signed BN.
   * @param value the BN to write to the stream
   * @param noBytes the number of bytes to write
   */
  writeSignedBigInteger(value: BN, noBytes: number): void;

  /**
   * Writes an unsigned 8-bit number.
   * @param value the number to write to the stream
   */
  writeU8(value: number): void;

  /**
   * Writes an unsigned 16-bit number.
   * @param value the number to write to the stream
   */
  writeU16(value: number): void;

  /**
   * Writes an unsigned 32-bit number.
   * @param value the number to write to the stream
   */
  writeU32(value: number): void;

  /**
   * Writes an unsigned 64-bit number.
   * @param value the BN to write to the stream
   */
  writeU64(value: BN): void;

  /**
   * Writes an unsigned BN.
   * @param value the BN to write to the stream
   * @param noBytes the number of bytes to write
   */
  writeUnsignedBigInteger(value: BN, noBytes: number): void;

  /**
   * Writes a string.
   * @param value the string to write to the stream
   */
  writeString(value: string): void;

  /**
   * Outputs the stream as a buffer.
   * @returns the buffer of the stream
   */
  toBuffer(): Buffer;
}
