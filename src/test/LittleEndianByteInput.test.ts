import { LittleEndianByteInput, BN } from "../main";

test("read I64", () => {
  const bytes = Buffer.from([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07]);
  const reader = new LittleEndianByteInput(bytes);
  expect(reader.readI64().eq(new BN("0706050403020100", "hex"))).toBeTruthy();
});

test("read U64", () => {
  const bytes = Buffer.from([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07]);
  const reader = new LittleEndianByteInput(bytes);
  expect(reader.readU64().eq(new BN("0706050403020100", "hex"))).toBeTruthy();
});

test("read I32", () => {
  const bytes = Buffer.from([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07]);
  const reader = new LittleEndianByteInput(bytes);
  expect(reader.readI32()).toEqual(0x03020100);
  expect(reader.readI32()).toEqual(0x07060504);
});

test("read U32", () => {
  const bytes = Buffer.from([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07]);
  const reader = new LittleEndianByteInput(bytes);
  expect(reader.readU32()).toEqual(0x03020100);
  expect(reader.readU32()).toEqual(0x07060504);
});

test("read I8", () => {
  const bytes = Buffer.from([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07]);
  const reader = new LittleEndianByteInput(bytes);
  expect(reader.readI8()).toEqual(0x00);
  expect(reader.readI8()).toEqual(0x01);
  expect(reader.readI8()).toEqual(0x02);
  expect(reader.readI8()).toEqual(0x03);
  expect(reader.readI8()).toEqual(0x04);
  expect(reader.readI8()).toEqual(0x05);
  expect(reader.readI8()).toEqual(0x06);
  expect(reader.readI8()).toEqual(0x07);
});

test("read U8", () => {
  const bytes = Buffer.from([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07]);
  const reader = new LittleEndianByteInput(bytes);
  expect(reader.readU8()).toEqual(0x00);
  expect(reader.readU8()).toEqual(0x01);
  expect(reader.readU8()).toEqual(0x02);
  expect(reader.readU8()).toEqual(0x03);
  expect(reader.readU8()).toEqual(0x04);
  expect(reader.readU8()).toEqual(0x05);
  expect(reader.readU8()).toEqual(0x06);
  expect(reader.readU8()).toEqual(0x07);
});

test("read I16", () => {
  const bytes = Buffer.from([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07]);
  const reader = new LittleEndianByteInput(bytes);
  expect(reader.readI16()).toEqual(0x0100);
  expect(reader.readI16()).toEqual(0x0302);
  expect(reader.readI16()).toEqual(0x0504);
  expect(reader.readI16()).toEqual(0x0706);
});

test("read U16", () => {
  const bytes = Buffer.from([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07]);
  const reader = new LittleEndianByteInput(bytes);
  expect(reader.readU16()).toEqual(0x0100);
  expect(reader.readU16()).toEqual(0x0302);
  expect(reader.readU16()).toEqual(0x0504);
  expect(reader.readU16()).toEqual(0x0706);
});

test("read bool", () => {
  const bytes = Buffer.from([0x00, 0x01, 0x02]);
  const reader = new LittleEndianByteInput(bytes);
  expect(reader.readBoolean()).toBeFalsy();
  expect(reader.readBoolean()).toBeTruthy();
  expect(reader.readBoolean()).toBeTruthy();
});

test("read bytes", () => {
  const bytes = Buffer.from([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07]);
  const reader = new LittleEndianByteInput(bytes);
  expect(reader.readBytes(6)).toEqual(bytes.slice(0, 6));
  expect(reader.readBytes(2)).toEqual(bytes.slice(6, 8));
});

test("read too many bytes", () => {
  const bytes = Buffer.from([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07]);
  const reader = new LittleEndianByteInput(bytes);
  expect(() => reader.readBytes(16)).toThrowError("Unable to read bytes");
});

test("read signed big integer", () => {
  const bytes = Buffer.from([-2, -1, -1, -1]);
  const reader = new LittleEndianByteInput(bytes);
  expect(reader.readSignedBigInteger(4).eqn(-2)).toBeTruthy();
});

test("read unsigned big integer", () => {
  const bytes = Buffer.from([-2, -1, -1, -1]);
  const reader = new LittleEndianByteInput(bytes);
  expect(reader.readUnsignedBigInteger(4).eq(new BN("fffffffe", "hex"))).toBeTruthy();
});

test("read string", () => {
  const bytes = Buffer.from([6, 0, 0, 0, 65, 66, 67, 32, 66, 66]);
  const reader = new LittleEndianByteInput(bytes);
  expect(reader.readString()).toEqual("ABC BB");
});

test("read remaining", () => {
  const bytes = Buffer.from([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07]);
  const reader = new LittleEndianByteInput(bytes);
  reader.readI16();
  expect(reader.readRemaining()).toEqual(Buffer.from([2, 3, 4, 5, 6, 7]));
});

test("skip bytes", () => {
  const bytes = Buffer.from([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07]);
  const reader = new LittleEndianByteInput(bytes);
  reader.skipBytes(3);
  expect(reader.readRemaining()).toEqual(Buffer.from([3, 4, 5, 6, 7]));
});

test("skip to end", () => {
  const bytes = Buffer.from([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07]);
  const reader = new LittleEndianByteInput(bytes);
  reader.skipBytes(8);
  expect(reader.readRemaining()).toEqual(Buffer.from([]));
});

test("skip too many", () => {
  const bytes = Buffer.from([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07]);
  const reader = new LittleEndianByteInput(bytes);
  expect(() => reader.skipBytes(10)).toThrowError("Cannot skip past buffer size");
});

test("skip negative", () => {
  const bytes = Buffer.from([0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07]);
  const reader = new LittleEndianByteInput(bytes);
  reader.skipBytes(4);
  reader.skipBytes(0);
  expect(() => reader.skipBytes(-2)).toThrowError("Must skip a non negative number of bytes");
});
